package ru.mustakimov.englishdictionary.model

import com.google.gson.annotations.SerializedName

data class UrbanDictionaryResponse(
    @field:SerializedName("list")
    val list: List<DictionaryListItem> = listOf()
)