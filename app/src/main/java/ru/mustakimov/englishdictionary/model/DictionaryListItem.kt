package ru.mustakimov.englishdictionary.model

import com.google.gson.annotations.SerializedName
import java.net.URL
import java.util.*

data class DictionaryListItem(

    @field:SerializedName("defid")
    val definitionId: Int = 0,

    @field:SerializedName("sound_urls")
    val soundUrls: List<URL> = listOf(),

    @field:SerializedName("thumbs_down")
    val thumbsDown: Int = 0,

    @field:SerializedName("author")
    val author: String = "",

    @field:SerializedName("written_on")
    val writtenOn: Date? = null,

    @field:SerializedName("definition")
    val definition: String = "",

    @field:SerializedName("permalink")
    val permalink: URL? = null,

    @field:SerializedName("thumbs_up")
    val thumbsUp: Int = 0,

    @field:SerializedName("word")
    val word: String = "",

    @field:SerializedName("current_vote")
    val currentVote: String? = null,

    @field:SerializedName("example")
    val example: String = ""
)