package ru.mustakimov.englishdictionary.dto

import java.net.URL

data class WordDefinition(
    val id: Int,
    val word: String,
    val author: String,
    val thumbsUp: Int,
    val thumbsDown: Int,
    val definition: String,
    val example: String,
    val soundUrls: List<URL>
)