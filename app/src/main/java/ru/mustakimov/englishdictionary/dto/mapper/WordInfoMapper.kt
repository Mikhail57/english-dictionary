package ru.mustakimov.englishdictionary.dto.mapper

import ru.mustakimov.englishdictionary.dto.WordDefinition
import ru.mustakimov.englishdictionary.model.UrbanDictionaryResponse

fun UrbanDictionaryResponse.toListOfWordDefinitions(): List<WordDefinition> = this.list.map {
    WordDefinition(
        id = it.definitionId,
        word = it.word,
        definition = it.definition,
        example = it.example,
        author = it.author,
        thumbsUp = it.thumbsUp,
        thumbsDown = it.thumbsDown,
        soundUrls = it.soundUrls
    )
}